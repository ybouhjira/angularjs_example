function customerController($scope) {
	$scope.customers = [
        {firstName: 'Karim', lastName: 'Adid', phone: '06 47 23 12 11' , email : 'karim@gmail.com'},
        {firstName: 'Saad', lastName: 'Karoum', phone: '06 10 99 12 33'  , email : 'saad@gmail.com'},
        {firstName: 'Soufian', lastName: 'Dakhishi', phone: '06 23 33 00 11', email : 'soufian@gmail.com'},
    ];
    $scope.remove = function(index) {
        if(confirm('Are you sure you want to delete line ' + index)) {
            $scope.customers.splice(index, 1);
        }
    };
    $scope.add = function() {
        $scope.customers.push({firstName: '', lastName: '', phone: '' , email : ''});
    };
    $scope.setOrder = function(order) {
        $scope.order = order ; 
        $scope.reverse = ($scope.reverse)? false : true;
    }
}

